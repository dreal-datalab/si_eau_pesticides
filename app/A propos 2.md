# Les stations de mesures
<img src="Vaudelle_53.jpg" alt="Vaudelle" class="apropos">

Les stations de mesures des pesticides où sont effectués les prélèvements appartiennent à trois réseaux :
  
1. le réseau de suivi de l’Agence Régionale de Santé, destiné principalement au contrôle des teneurs en pesticides vis-à-vis de la production d’eau potable ;

2. le réseau de suivi de l’Agence de l’Eau Loire Bretagne, destiné à la connaissance générale de l’état des cours d’eau ;

3. le réseau de suivi complémentaire régional, qui cible 38 stations sur lesquelles il a été identifié l’intérêt de disposer de chroniques de données à fréquence suffisante (au moins un prélèvement par mois). Ce réseau recoupe en partie les deux précédents, des compléments d’analyses étant réalisés pour parvenir à la fréquence souhaitée. Il est qualifié de **réseau patrimonial**. Le suivi de ce réseau a été suspendu fin 2019.  

<h5> <a href="station_ref.xlsx" target="_blank">Télécharger la liste des 38 stations du réseau patrimonial</a></h3>
