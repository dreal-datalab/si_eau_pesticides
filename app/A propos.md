# L'application

Cette application met à disposition les données sur les pesticides dans les cours d'eau de la région des Pays de la Loire à partir de 2011 et propose des indicateurs pour visualiser leur évolution et leur répartition.

Ces données sont issues de l'Agence de l'Eau Loire Bretagne, de l'Agence Régionale de Santé et du réseau complémentaire de suivi régional financé par la DREAL, la DRAAF et l'Agence de l'Eau sur la période 2011-2019. Le suivi de ce réseau a été suspendu fin 2019.

Ces données seront mises à jour régulièrement et l'application enrichie au fur et à mesure.

N'hésitez pas à [nous contacter](mailto:srnp.dreal-paysdelaloire@developpement-durable.gouv.fr?subject=Application pesticides&body=Bonjour,") pour toute remarque ou suggestion.

